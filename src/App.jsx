import React from "react";
import { Route, Routes } from "react-router-dom";
import Election from "./pages/Election";
import HomePage from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";

export const App = () => {
  return (
    <Routes>
      <Route path="/" element={<HomePage />}></Route>
      <Route path="/register" element={<Register />}></Route>
      <Route path="/login" element={<Login />}></Route>
      <Route path="/election" element={<Election />}></Route>
    </Routes>
  );
};
