import React from "react";
import Sidebar from "../components/Sidebar";

const MainLayout = ({ children }) => {
  return (
    <div className="relative">
      <Sidebar />
      <main className="w-4/5 absolute right-0 top-0 px-6 py-4">{children}</main>
    </div>
  );
};

export default MainLayout;
