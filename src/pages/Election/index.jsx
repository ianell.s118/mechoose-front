import React from "react";
import { Button } from "../../components/Buttons";

const Election = () => {
  return (
    <div>
      <Button>Créer une élection</Button>
      <div className="px-2 py-5 shadow-md mt-8">
        <span className="px-6">1) Meilleure artiste féminine</span>
      </div>
      <div className="px-2 py-5 shadow-md mt-8">
        <span className="px-6">2) Meilleure jeune</span>
      </div>
    </div>
  );
};

export default Election;
