import React from "react";
import { Link } from "react-router-dom";
import Input from "../../components/Input";

const Login = () => {
  return (
    <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 w-1/4">
      <form className="w-full">
        <legend className="text-5xl text-center">Se connecter</legend>
        <div className="mt-5">
          <label>Email: </label>
          <br />
          <Input
            className="px-4 w-full"
            type="email"
            placeholder="Entrer votre email"
          />
        </div>
        <div className="mt-5">
          <label>Mot de passe: </label>
          <br />
          <Input
            className="px-4 w-full"
            type="password"
            placeholder="Entrer votre mot de passe"
          />
        </div>
        <Input
          className="border-none bg-blue-500 text-white w-full my-5"
          type="submit"
          value="Se connecter"
          bordercolor="none"
        />
        <div className="text-center">
          <span className="text-lg">
            Vous n'avez pas de compte ?{" "}
            <Link className="text-blue-500 underline" to={"/"}>
              S'inscrire
            </Link>
          </span>
        </div>
      </form>
    </div>
  );
};

export default Login;
