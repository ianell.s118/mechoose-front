import React from "react";
import { Link } from "react-router-dom";
import Input from "../../components/Input";

const Register = () => {
  return (
    <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 w-1/4">
      <form className="w-full">
        <legend className="text-5xl text-center">S'inscrire</legend>
        <div className="mt-5">
          <label>Nom: </label>
          <br />
          <Input
            className="px-4 w-full"
            type="text"
            placeholder="Entrer votre nom"
          />
        </div>
        <div className="mt-5">
          <label>Email: </label>
          <br />
          <Input
            className="px-4 w-full"
            type="email"
            placeholder="Entrer votre email"
          />
        </div>
        <div className="mt-5">
          <label>Mot de passe: </label>
          <br />
          <Input
            className="px-4 w-full"
            type="password"
            placeholder="Entrer votre mot de passe"
          />
        </div>
        <Input
          className="border-none bg-blue-500 text-white w-full my-5"
          type="submit"
          value="S'inscrire"
          bordercolor="none"
        />
        <div className="text-center">
          <span className="text-lg">
            Vous avez-deja un compte ?{" "}
            <Link className="text-blue-500 underline" to={"/login"}>
              Se connecter
            </Link>
          </span>
        </div>
      </form>
    </div>
  );
};

export default Register;
