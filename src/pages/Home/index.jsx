import React from "react";
import ElectionCard from "../../components/ElectionCard";
import ResultCard from "../../components/ResultCard";

const HomePage = () => {
  return (
    <div>
      <h1 className="text-3xl text-center">Résultats des élections: </h1>
      <div>
        <ElectionCard />
        <ElectionCard />
      </div>
    </div>
  );
};

export default HomePage;
