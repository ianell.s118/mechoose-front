import React from "react";
import { Link } from "react-router-dom";

const Sidebar = () => {
  return (
    <div className="fixed top-0 left-0 h-screen w-1/5 flex flex-col p-4 bg-blue-500 text-white">
      <h1 className="text-center text-5xl mb-12 mt-5">Me choose</h1>
      <Link
        className="text-2xl px-5 py-2 hover:bg-white hover:text-blue-500"
        to={"/"}
      >
        Accueil
      </Link>
      <Link
        className="text-2xl px-5 py-2 hover:bg-white hover:text-blue-500"
        to={"/election"}
      >
        Elections
      </Link>
    </div>
  );
};

export default Sidebar;
