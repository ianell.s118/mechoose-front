import React from "react";
import { VictoryChart, VictoryBar, VictoryTheme } from "victory";

const ResultCard = (props) => {
  return (
    <div className="block border-blue-500 border-2 w-2/3 p-12 my-5 mx-auto h-2/5">
      <VictoryChart horizontal theme={VictoryTheme.material} domainPadding={60}>
        <VictoryBar
          alignment="middle"
          style={{ data: { fill: "#c43a31" } }}
          data={props.data}
          labels={({ datum }) => `${datum.y}%`}
        />
      </VictoryChart>
    </div>
  );
};

export default ResultCard;
