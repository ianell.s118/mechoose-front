import React from "react";
import ResultCard from "../ResultCard";

const ElectionCard = () => {
  const data1 = [
    { x: "Denise", y: 56 },
    { x: "Johane", y: 40 },
    { x: "Oashna", y: 4 },
  ];

  const data2 = {};

  return (
    <div className="m-3">
      <h2 className="text-2xl text-blue-500">Meilleure artiste féminine</h2>
      <ResultCard data={data1} />
    </div>
  );
};

export default ElectionCard;
